Objective: 

1. Code Review : review each other's homework with my teammates , Incorporate feedback from teammates to modify my code, such as catching specific exception.
1. Draw a TDD concept map with my teammates, it helps me review what I have learned. 
1. Learn http basic and restful api specification. 
1. Pair Programming: Learn what is pair programming and practice it with teammate.  
1. Spring Boot: build basic api in controller layer and use postman to call api.  

Reflective:  Fruitful and Challenging.

Interpretive:  During this day of study, I learned a lot about Spring Boot.

Decisional:  I'm going to learn more about  Spring Boot!
