package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee createEmployee(Employee employee){
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Employee getEmployeeById(Long id){
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender){
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id,Employee inputEmployee){
        Employee qureyEmployee = getEmployeeById(id);
        if(!Objects.isNull(qureyEmployee)) {
            qureyEmployee.setAge(inputEmployee.getAge());
            qureyEmployee.setSalary(inputEmployee.getSalary());
            qureyEmployee.setCompanyId(inputEmployee.getCompanyId());
        }
        return qureyEmployee;
    }

    public List<Employee> pageQuery(int page, int size){
        if(page <= 0 || size <= 0){
            return null;
        }
        return employees.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public void deleteEmployeeById(@PathVariable Long id){
        Employee qureyEmployee = employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst().orElse(null);
        employees.remove(qureyEmployee);
    }

    public List<Employee> getAllEmployees(){
        return this.employees;
    }

    public Long generateId(){
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1).orElse(1L);
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> companyId.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }

    public void deleteEmployeeByCompanyId(Long companyId) {
        List<Employee> employeeList = employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
        employeeList.forEach(employees::remove);
    }

    public void clear() {
        employees.clear();
    }
}
