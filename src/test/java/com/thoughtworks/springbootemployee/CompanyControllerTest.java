package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc client;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @BeforeEach
    void setUp(){
        employeeRepository.clear();
        companyRepository.clear();
    }

    @Test
    void should_get_all_companies_when_perform_get_all_companies_given_companies() throws Exception{
    //given
        Company first = new Company(1L, "first");
    //when
    //then
        companyRepository.createCompany(first);
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("first"));
    }

    @Test
    void should_get_company_when_perform_get_company_by_id_given_companies() throws Exception{
        //given
        Company first = new Company(1L, "first");
        //when
        //then
        companyRepository.createCompany(first);
        client.perform(MockMvcRequestBuilders.get("/companies/{id}",first.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("first"));
    }

    @Test
    void should_create_company_when_perform_create_company_given_company() throws Exception {
        //given
        Company first = new Company(1L, "first");
        String companyJson = new ObjectMapper().writeValueAsString(first);
        //when
        //then
        client.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("first"));
    }

    @Test
    void should_get_all_employees_by_page_when_perform_get_all_employees_by_page_given_employees() throws Exception {
        //given
        Company company1 = new Company(1L, "scala");
        Company company2 = new Company(2L, "java");
        Company company3 = new Company(3L, "python");
        Company company4 = new Company(4L, "spring");
        companyRepository.createCompany(company1);
        companyRepository.createCompany(company2);
        companyRepository.createCompany(company3);
        companyRepository.createCompany(company4);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/companies")
                .param("page", "1")
                .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("scala"));

    }

    @Test
    void should_delete_company_when_perform_delete_employee_given_employee() throws Exception {
        //given
        Company company = new Company(1l, "spring");
        //when
        //then
        companyRepository.createCompany(company);
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertEquals(0, companyRepository.getAllCompanies().size());
    }

    @Test
    void should_update_company_when_perform_update_company_given_company() throws Exception {
        //given
        Company company = new Company(1L, "first");
        Company newCompany = new Company(1L, "second");
        String newCompanyJson = new ObjectMapper().writeValueAsString(newCompany);
        //when
        //then
        companyRepository.createCompany(company);
        client.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId())
                .param("name",newCompany.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .content(newCompanyJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(newCompany.getName()));
    }

    @Test
    void should_get_all_employees_by_company_id_when_perform_get_employees_by_company_id_given_employees_and_company_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "jeff", 22, "male", 1111, 1L);
        Employee employee2 = new Employee(2L, "manman", 22, "female", 5000, 1L);
        Employee employee3 = new Employee(3L, "canqiana", 21, "male", 8888, 2L);
        Company company = new Company(1L, "spring");

        companyRepository.createCompany(company);
        employeeRepository.createEmployee(employee1);
        employeeRepository.createEmployee(employee2);
        employeeRepository.createEmployee(employee3);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("jeff"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(22));

    }


}
