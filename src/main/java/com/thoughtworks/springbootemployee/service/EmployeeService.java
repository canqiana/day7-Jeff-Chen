package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee createEmployee(Employee employee) {
        return employeeRepository.createEmployee(employee);
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.getAllEmployees();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.getEmployeeById(id);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeRepository.getEmployeeByGender(gender);
    }

    public Employee updateEmployee(Long id,Employee inputEmployee) {
        return employeeRepository.updateEmployee(id,inputEmployee);
    }

    public void deleteEmployeeById(Long id) {
        employeeRepository.deleteEmployeeById(id);
    }

    public List<Employee> pageQuery(int page, int size) {
        return employeeRepository.pageQuery(page,size);
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId){
        return employeeRepository.getEmployeesByCompanyId(companyId);
    }
}
