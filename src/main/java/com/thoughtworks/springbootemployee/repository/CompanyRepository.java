package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    @Autowired
    private EmployeeRepository employeeRepository;

    private final List<Company> companies = new ArrayList<>();


    public Long generateId(){
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(company -> company.getId() + 1).orElse(1L);
    }

    public Company createCompany(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    public List<Company> getAllCompanies() {
        return this.companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public Company updateCompany(Long id, String name) {
        Company qureyCompany = getCompanyById(id);
        if(Objects.nonNull(qureyCompany)){
            qureyCompany.setName(name);
        }
        return qureyCompany;
    }

    public List<Company> pageQuery(int page, int size) {
        if(page <= 0 || size <= 0){
            return null;
        }
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public void deleteCompanyById(Long companyId) {
        Company qureyCompany = getCompanyById(companyId);
        companies.remove(qureyCompany);
        employeeRepository.deleteEmployeeByCompanyId(companyId);

    }

    public void clear() {
        companies.clear();
    }
}
