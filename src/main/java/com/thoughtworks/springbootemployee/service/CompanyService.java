package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public Company createCompany(Company company) {
        return companyRepository.createCompany(company);
    }

    public List<Company> getAllCompanies() {
        return companyRepository.getAllCompanies();
    }

    public Company getCompanyById(Long id) {
        return companyRepository.getCompanyById(id);
    }

    public Company updateCompany(Long id, String name) {
        return companyRepository.updateCompany(id,name);
    }

    public List<Company> pageQuery(int page, int size) {
        return companyRepository.pageQuery(page,size);
    }

    public void deleteCompanyById(Long companyId) {
        companyRepository.deleteCompanyById(companyId);
    }
}
