package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc client;
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp(){
        employeeRepository.clear();
    }

    @Test
    void should_get_all_employees_when_perform_get_all_employees_given_employees() throws Exception {
    //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee);
    //when  then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(3000));
    }

    @Test
    void should_get_employee_when_perform_get_employee_by_id_given_employees() throws Exception{
    //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee);
     //when  then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}",employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
    }

    @Test
    void should_create_employee_when_perform_create_employee_given_employees() throws Exception{
    //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
    //when then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
    }

    @Test
    void should_get_employees_when_perform_find_employees_by_gender_given_employees() throws Exception {
    //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee);
     //when  then
        client.perform(MockMvcRequestBuilders.get("/employees?gender={gender}",employee.getGender()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(3000));
    }

    @Test
    void should_get_employee_when_perform_update_employee_given_employees() throws Exception {
    //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee);
        String employeeJson = new ObjectMapper().writeValueAsString(new Employee(1L, "Lili", 30, "male", 5000));
     //when

     //then
        client.perform(MockMvcRequestBuilders.put("/employees/{id}",employee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(30))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_delete_employee_when_perform_delete_employee_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee);
        //when then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}",employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertEquals(0,employeeRepository.getAllEmployees().size());
    }

    @Test
    void should_get_employees_when_perform_find_by_page_given_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lili", 20, "male", 3000);
        Employee employee2 = new Employee(2L, "Lili", 20, "male", 3000);
        employeeRepository.createEmployee(employee1);
        employeeRepository.createEmployee(employee2);
        //when then
        client.perform(MockMvcRequestBuilders.get("/employees?page={page}&&size={size}",1,2))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(2)));
    }

}
